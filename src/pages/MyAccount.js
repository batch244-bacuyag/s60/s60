import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';

export default function AccountInformation(user){

  // State that will be used to store the user information retrieved from the database
  const [userDetails, setUserDetails] = useState({});
  const { firstName, lastName, email, mobileNo, orders } = userDetails;

  // Fetches the user information from the database upon initial render of the component
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      }
    })
    .then(res => res.json())
    .then(data => {
      // Sets the "userDetails" state to the fetched user information
      setUserDetails(data);
      console.log(data)
    })
  }, []);


  return  (
    <div>
      <h3>Account Information</h3>
      <br></br>
        <Card bg="info">
          <Card.Body>  
            <Card.Text>
              First Name: <strong>{firstName || user.firstName}</strong>
            </Card.Text>        
            <Card.Text>
              Last Name: <strong>{lastName || user.lastName}</strong>
            </Card.Text>
            <Card.Text>
              Mobile Number: <strong>0{mobileNo || user.mobileNo}</strong>
            </Card.Text>
            <Card.Text>
              Email: <strong>{email || user.email}</strong>
            </Card.Text>            
          </Card.Body>
        </Card>
        <br></br>
        <h3>Orders</h3>
        {orders && orders.length > 0 && (
          <div style={{ marginTop: '20px' }}>
            {orders
              .filter(order => order.purchaseOn !== null)
              .map(order => (
                <Card key={order._id} style={{ border: '1px solid grey', marginBottom: '20px' }}>
                  <Card.Header>Order ID: {order._id}</Card.Header>
                  <Card.Body>
                    <Card.Text>
                      Date Ordered: {new Date(order.purchaseOn).toLocaleString()}
                    </Card.Text>
                    <Card.Text>
                      Products:
                      <ul>
                        {order.products.map(product => (
                          <li key={product.productId}>
                            {product.productName} ({product.quantity} {product.quantity === 1 ? 'unit' : 'units'})
                          </li>
                        ))}
                      </ul>
                    </Card.Text>
                    <Card.Text>
                      Total Amount: {order.totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}
                    </Card.Text>
                  </Card.Body>
                </Card>
              ))}
          </div>
        )}
    </div>
  );
}
