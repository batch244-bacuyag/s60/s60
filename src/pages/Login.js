import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';

export default function Login() {

    // Allows us to consume the User context object and its properties to use for user validation
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request from the correspondinf backend API
        // The header information "Content-type" is used to specify that the info being sent to the backend will be sent in the form JSON
        // The fetch request will communicate with our BE application providing it with a stringified JSON
        // Convert the info into JS object using .then => (res.json())
        // Capture the converted data and using the ".then(data => {})"
        // Syntax:
            /*
                fetch('url', {options})
                .then(res => res.json())
                .then(data => {})
            
            */ 

        fetch('http://localhost:4001/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.access)

            if(data.access !== undefined) {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Sneakpick"
                });
            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })


        // Set the email of the authenticated user in the local storage
        // Syntax: localStorage.setItem('propertyName', value)
        // localStorage.setItem('email', email);

        // Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
        // When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering

        // setUser({email: localStorage.getItem('email')});

        // Clear input fields after submission
        setEmail('');
        setPassword('');

        // alert(`You are now logged in.`)

    }

    // Retrieving user details

    const retrieveUserDetails = (token) => {

        fetch('http://localhost:4001/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global user state tos tore the "id" and the "idAdmin" property of the user which will be used for validation across the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (

        //Conditional rendering that will redirect the user to the courses page when a user is logged in 
        (user.id !== null)
        ?
            <Navigate to='/'/>
        :
        // Invokes the authenticate function upon clicking on submit button
        <div style={{ display: 'flex', justifyContent: 'center', height: '48vh' }}>
          <Form onSubmit={(e) => authenticate(e)} style={{ width: '400px', border: '1px solid grey', borderRadius: '10px', padding: '20px' }}>
            <h3>Sign-in</h3>
            <br></br>
            <Form.Group controlId="userEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control 
                type="email" 
                placeholder="Enter email" 
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                style={{ width: '100%' }}
              />
            </Form.Group>
            <br></br>
            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                style={{ width: '100%' }}
              />
            </Form.Group>
            <br></br>
            { isActive 
              ? 
                <Button variant="primary" type="submit" id="submitBtn" style={{ width: '100%' }}>
                  Submit
                </Button>
              : 
                <Button variant="danger" type="submit" id="submitBtn" disabled style={{ width: '100%' }}>
                  Submit
                </Button>
            }
            <br></br>
            <br></br>
            <br></br>
            <Form.Label style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <div style={{flex: '1', height: '1px', backgroundColor: 'black'}}></div>
              <span style={{padding: '0 10px'}}>New to TechTonic?</span>
              <div style={{flex: '1', height: '1px', backgroundColor: 'black'}}></div>
            </Form.Label>
            <br></br>
            <Button variant="secondary" /*type="submit" id="registerBtn" */
              style={{ 
                width: '100%',
                backgroundColor: 'white',
                color: '#ae4343',
                borderColor: '#ae4343',
                fontWeight: 'bold',
                borderStyle: 'solid',
                borderWidth: '3px',
              }}>
              Create your TechTonic account
            </Button>
          </Form>
        </div>
    )
}
